伤寒论查阅iOS版重构
=======

### 本项目采用storyboard, 基于Xcode 8

### 本应用为中医查询类应用

### 使用技术介绍
1. 采用了自我设计的染色标记，自己解析实现；
2. 数据使用json存储，使用OC的runtime解析
3. 搜索筛选，数据处理采用自己实现的map reduce扩展

## 本应用为非盈利目的而做，因此开源，因为有医学内容，请慎重对待，不可轻试。内容采用宋版伤寒论，但是尚未完全校对完毕
