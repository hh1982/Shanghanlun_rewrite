//
//  NSArray+Filter.m
//  renrenPrint
//
//  Created by hh on 16/7/19.
//  Copyright © 2016年 hh. All rights reserved.
//

#import "NSArray+Filter.h"

@implementation NSArray (Filter)

- (NSArray *)filter:(FilterArrayBlock)block
{
    NSMutableArray *arr = [NSMutableArray new];
    for (id obj in self) {
        if (block(obj)) {
            [arr addObject:obj];
        }
    }
    return arr;
}

- (NSArray *)filter:(FilterArrayBlock)fBlock thenMap:(MapArrayBlock)mBlock
{
    NSMutableArray *arr = [NSMutableArray new];
    for (id obj in self) {
        if (fBlock(obj)) {
            [arr addObject:mBlock(obj)];
        }
    }
    return arr;
}

- (void)filter:(FilterArrayBlock)fBlock thenForEachDo:(ForEachBlock)dBlock
{
    for (id obj in self) {
        if (fBlock(obj)) {
            dBlock(obj);
        }
    }
}

- (NSArray *)uniqArray
{
    NSMutableArray *arr = [NSMutableArray new];
    for (id obj in self) {
        if (![arr containsObject:obj]) {
            [arr addObject:obj];
        }
    }
    return arr;
}

- (NSArray *)map:(Map)block
{
    NSMutableArray *arr = [NSMutableArray new];
    int i = 0;
    for (id obj in self) {
        [arr addObject:block(obj, i++)];
    }
    return arr;
}

- (NSArray *)map:(Map)mBlock thenFilter:(FilterArrayBlock)fBlock
{
    NSMutableArray *arr = [NSMutableArray new];
    int i = 0;
    for (id obj in self) {
        id res = mBlock(obj, i++);
        if (fBlock(res)) {
            [arr addObject:res];
        }
    }
    return arr;
}

- (NSArray *)mapArrayWithBlock:(MapArrayBlock)block
{
    NSMutableArray *arr = [NSMutableArray new];
    for (id obj in self) {
        [arr addObject:block(obj)];
    }
    return arr;
}

- (id)reduceArrayWithBlock:(ReduceArrayBlock)block initValue:(id)origin
{
    id res = origin;
    for (id obj in self) {
        res = block(obj, res);
    }
    return res;
}

- (NSInteger)findObjIndexWithBlock:(FindObjBlock)block
{
    for (int i = 0; i < self.count; i++) {
        if (block(self[i])) {
            return i;
        }
    }
    return -1;
}

- (void)forEachDo:(ForEachBlock)block
{
    for (id obj in self) {
        block(obj);
    }
}

- (void)forEach:(ForEach)block
{
    int i = 0;
    for (id obj in self) {
        block(obj, i++);
    }
}

- (BOOL)some:(BoolBlock)block
{
    for (id obj in self) {
        if (block(obj)) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)every:(BoolBlock)block
{
    for (id obj in self) {
        if (!block(obj)) {
            return NO;
        }
    }
    return YES;
}

- (void)gridForCount:(NSInteger)count colums:(NSInteger)cols do:(IntBlock)block
{
    NSAssert(cols > 0, @"cols 必须大于等于0！");
    for (NSInteger i = 0; i < count; i++) {
        NSInteger row = i/cols;
        NSInteger col = i - row*cols;
        block(row, col, i);
    }
}

+ (NSArray *)arrayFrom:(NSInteger)from to:(NSInteger)to
{
    BOOL less = from < to;
    NSMutableArray *arr = [NSMutableArray new];
    if (less) {
        for (NSInteger i = from; i <= to; i++) {
            [arr addObject:@(i)];
        }
    }else{
        for (NSInteger i = from; i >= to; i--) {
            [arr addObject:@(i)];
        }
    }
    return arr;
}

+ (NSArray *)arrayFrom:(NSInteger)from to:(NSInteger)to step:(NSInteger)step
{
    BOOL less = from < to;
    NSMutableArray *arr = [NSMutableArray new];
    if (less) {
        for (NSInteger i = from; i <= to; i += step) {
            [arr addObject:@(i)];
        }
    }else{
        for (NSInteger i = from; i >= to; i -= step) {
            [arr addObject:@(i)];
        }
    }
    return arr;
}

+ (NSArray *)arrayFor:(id)obj count:(NSInteger)count
{
    NSMutableArray *arr = [NSMutableArray new];
    for (int i = 0; i < count; i++) {
        [arr addObject:obj];
    }
    return arr;
}


@end
