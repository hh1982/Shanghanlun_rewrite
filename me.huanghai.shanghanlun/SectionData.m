//
//  SectionData.m
//  me.huanghai.shanghanlun
//
//  Created by hh on 16/10/11.
//  Copyright © 2016年 hh. All rights reserved.
//

#import "SectionData.h"
#import <UIKit/UIKit.h>
#import "NSArray+Filter.h"
#import "AppDelegate.h"

@implementation NodeStart
@end

@implementation NodeEnd
@end

@implementation ItemData
- (void)setValue:(id)value forKey:(NSString *)key
{
    if ([key isEqualToString:@"text"]) {
        if (![value respondsToSelector:@selector(substringFromIndex:)]) {
            self.text = value;
            return;
        }
        NSMutableString *text = [value mutableCopy];
        self.text = [[AppDelegate sharedApp] parseText:text];
        return;
    }
    [super setValue:value forKey:key];
}

@end

@implementation SectionData

- (Class)getSubDataClassInArrayForKey:(NSString *)key
{
    return [ItemData class];
}

@end
