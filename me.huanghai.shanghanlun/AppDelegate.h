//
//  AppDelegate.h
//  me.huanghai.shanghanlun
//
//  Created by hh on 16/10/11.
//  Copyright © 2016年 hh. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SectionData.h"
#import "NSArray+Filter.h"
#import "ContentCell.h"
#import "HH2SearchConfig.h"
#import "UIView+HH2Rect.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

// data
@property (strong,nonatomic) NSArray<SectionData *> *itemData;
@property (strong,nonatomic) NSDictionary<NSString *, UIColor *> *colorDict;

+ (instancetype)sharedApp;

- (NSMutableAttributedString *)parseText:(NSMutableString *)text;

@end

