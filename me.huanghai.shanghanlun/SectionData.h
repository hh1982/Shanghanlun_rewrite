//
//  SectionData.h
//  me.huanghai.shanghanlun
//
//  Created by hh on 16/10/11.
//  Copyright © 2016年 hh. All rights reserved.
//

#import "JSONModel.h"

@interface NodeStart : NSObject
@property (assign, nonatomic) NSInteger start;
@property (assign, nonatomic) NSInteger end;
@property (strong, nonatomic) NSString *cls;
@end

@interface NodeEnd : NSObject
@property (assign, nonatomic) NSInteger end;
@end

@interface ItemData : JSONModel
@property (assign,nonatomic) NSInteger ID;
@property (strong,nonatomic) NSMutableAttributedString *text;
@property (strong,nonatomic) NSArray<NSString *> *fangList;
@property (strong,nonatomic) NSArray<NSString *> *yaoList;
@property (assign,nonatomic) float height;
@end

@interface SectionData : JSONModel

@property (assign,nonatomic) NSInteger section;
@property (strong,nonatomic) NSString *header;
@property (strong,nonatomic) NSArray<ItemData *> *data;

@end
